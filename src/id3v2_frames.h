#ifndef ID3V2_FRAMES_H
#define ID3V2_FRAMES_H

#include "id3v2.h"

/*
<Header for 'Text information frame', ID: "T000" - "TZZZ", excluding "TXXX" described in 4.2.2.>
Text encoding    $xx
Information    <text string according to encoding>
*/

class ID3v2_Frame_Text : public ID3v2_Frame {

public:

	ID3v2_Frame_Text(unsigned tagVersion, const char* frameID, unsigned frameSize, unsigned flags, streamoff fileOff)
		: ID3v2_Frame(tagVersion, frameID, frameSize, flags, fileOff), m_textEncoding(0) {}

	char getTextEncoding() const { return m_textEncoding; }
	const char* getText() const { return m_data; }

	void setTextEncoding(char enc) { m_textEncoding = enc; }
	void setText(const char* text, size_t len) { setData(text, len); }

private:

	char m_textEncoding;

};

#endif