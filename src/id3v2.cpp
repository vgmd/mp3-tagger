#include "id3v2.h"
#include "id3v2_frames.h"

#include <iostream>
#include <string>

// Converts an unsigned int to a 32-bit synchsafe int
// If the uint is greater than 2^28-1, the synchsafe int is capped to 2^28-1
unsigned uintToSynchsafe(unsigned i) {
	if (i > SYNCHSAFE32_MAX)
		i = SYNCHSAFE32_MAX;
	unsigned byte[4];
	byte[0] = (i & 0x0fffffff) >> 21;
	byte[1] = (i & 0x001fffff) >> 14;
	byte[2] = (i & 0x00003fff) >> 7;
	byte[3] = i & 0x0000007f;
	return byte[0] << 24 | byte[1] << 16 | byte[2] << 8 | byte[3];
}

// Converts a 32-bit synchsafe int to an unsigned int
unsigned synchsafeToUint(unsigned s) {
	unsigned byte[4];
	byte[0] = s >> 24;
	byte[1] = (s & 0x00ffffff) >> 16;
	byte[2] = (s & 0x0000ffff) >> 8;
	byte[3] = s & 0x000000ff;
	return byte[0] << 21 | byte[1] << 14 | byte[2] << 7 | byte[3];
}

// Converts 4 bytes representing a synchsafe int to an unsigned int
// Undefined behavior if a synchsafe int is not passed as a parameter
unsigned synchsafeToUint(const char* s) {
	unsigned byte[4];
	for (int i = 0; i < 4; i++)
		byte[i] = (unsigned char) s[i];
	return byte[0] << 21 | byte[1] << 14 | byte[2] << 7 | byte[3];
}

// Converts 4 bytes to an unsigned int
// Undefined behavior if 4 bytes aren't passed as a parameter
unsigned bytesToUint(const char* b) {
	unsigned byte[4];
	for (int i = 0; i < 4; i++)
		byte[i] = (unsigned char) b[i];
	return byte[0] << 24 | byte[1] << 16 | byte[2] << 8 | byte[3];
}

// Finds the tag, returning false if one can't be found. Guaranteed to keep the file in a good state.
bool ID3v2_findTag(streampos& pos, fstream& file) {
	char pattern[10];
	bool retvalue = false;

	// Check for prepended header
	file.seekg(0, ios::beg);
	pos = file.tellg();
	file.read(pattern, ID3V2_TAG_HDR_SIZE);
	if (pattern[0] == 0x49 && pattern[1] == 0x44 && pattern[2] == 0x33 &&
		pattern[3] < 0xff && pattern[4] < 0xff && pattern[6] < 0x80 &&
		pattern[7] < 0x80 && pattern[8] < 0x80 && pattern[9] < 0x80) {
			file.seekg(-ID3V2_TAG_HDR_SIZE, ios::cur);
			pos = file.tellg();
			retvalue = true;
	}

	/*
	// Check for footer
	file.seekg(10, ios::end);
	file.read(pattern, ID3V2_TAG_HDR_SIZE);
	if (pattern[0] == 0x33 && pattern[1] == 0x44 && pattern[2] == 0x49 &&
		pattern[3] < 0xff && pattern[4] < 0xff && pattern[6] < 0x80 &&
		pattern[7] < 0x80 && pattern[8] < 0x80 && pattern[9] < 0x80) {
			file.seekg(-ID3V2_TAG_HDR_SIZE, ios::cur);
			pos = file.tellg();
			retvalue = true;
	}
	*/

	file.clear();
	file.seekg(0, ios::beg);
	return retvalue;
}

// Parses the ID3v2 tag
ID3v2_Tag* ID3v2_parseTag(fstream& file) {

	streampos begin;
	char* tagData = 0;
	ID3v2_Tag* tag = 0;

	if (!ID3v2_findTag(begin, file))
		return false;

	//////////////////////////////
	// Parse the header

	streamoff tagOffset = begin;
	file.seekg(begin);
	char header[ID3V2_TAG_HDR_SIZE];
	file.read(header, ID3V2_TAG_HDR_SIZE);
	if (!file.good())
		return false;

	if (header[3] == 3) {

		///////////////////////////////
		// Parse version 3 tag
		
		// Version
		char* ver = header + 3;

		// Flags
		unsigned flags = header[5];
	
		// Tag Size
		unsigned tagSize = synchsafeToUint(header + 6);

		ID3v2_3_Tag* v3tag = new ID3v2_3_Tag(ver, flags, tagSize, tagOffset);

		// Read the rest of the tag into memory
		tagData = new char[v3tag->getTagSize()];
		file.read(tagData, v3tag->getTagSize());

		unsigned offset = 0; // Will be used to offset the beginning of the frame pointer

		if (v3tag->flagIsSet(ID3V2_E_HDR)) {

			// Header size
			unsigned eHeaderSize = bytesToUint(tagData);
			v3tag->setEHeaderSize(eHeaderSize);
			offset = 4 + eHeaderSize; // add 4 because eHeaderSize excludes itself

			// Extended flags
			unsigned eFlagByte0 = (unsigned char) tagData[4];
			unsigned eFlagByte1 = (unsigned char) tagData[5];
			v3tag->setEFlags(eFlagByte0 << 8 | eFlagByte1);

			// Padding size
			unsigned paddingSize = bytesToUint(tagData + 6);
			v3tag->setPaddingSize(paddingSize);

		} else {
			v3tag->setEHeaderSize(0);
			v3tag->setEFlags(0);
		}

		// Parse frames

		char* framePtr = tagData + offset;
		ID3v2_parseFrames(v3tag, framePtr);

		tag = v3tag;

	} else {

		printf("ID3v2.%i not supported\n", tag->getVersion()[0]);
		return 0;

	}
	
	delete[] tagData;
	return tag;
}


bool ID3v2_parseFrames(ID3v2_Tag* tag, char* framePtr) {

	streamoff currentOff = tag->getFileOff() + ID3V2_TAG_HDR_SIZE;

	if (tag->flagIsSet(ID3V2_E_HDR)) {
		switch (tag->getVersion()[0]) {
		case 3:
			currentOff += 4 + tag->getEHeaderSize();
			break;
		}
	}

	while (framePtr[0] != 0) {

		ID3v2_Frame* frame = 0;

		if (tag->getVersion()[0] == 3) {

			///////////////////////////
			// V3 Frame Parsing

			// GENERIC V3 HEADER INFO
			// Frame ID
			char frameID[4];
			memcpy(frameID, framePtr, ID3V2_3_FRAME_ID_LEN);

			// Frame size
			unsigned frameSize = bytesToUint(framePtr + 4);

			// Frame flags
			unsigned flagByte0 = (unsigned char) framePtr[8];
			unsigned flagByte1 = (unsigned char) framePtr[9];
			unsigned flags = flagByte0 << 8 | flagByte1;
			
			framePtr += ID3V2_3_FRAME_HDR_SIZE;

			if (frameID[0] == 'T') {

				///////////////////////////
				// Text information frames

				ID3v2_Frame_Text* tFrame = new ID3v2_Frame_Text(tag->getVersion()[0], frameID, frameSize, flags, currentOff);
				tFrame->setTextEncoding(framePtr[0]);
				tFrame->setText(framePtr + 1, tFrame->getFrameSize() - 1);

				frame = tFrame;

			} else {

				///////////////////////////
				// Generic non-supported frame

				frame = new ID3v2_Frame(tag->getVersion()[0], frameID, frameSize, flags, currentOff);
				frame->setData(framePtr, frame->getFrameSize());

			}

		} else {
			printf("ID3v2.%i not supported\n", tag->getVersion()[0]);
		}

		// Add frame to tag map
		tag->addFrame(frame);

		framePtr += frame->getFrameSize();
		switch(tag->getVersion()[0]) {
		case 2:
			currentOff += frame->getFrameSize() + ID3V2_2_FRAME_HDR_SIZE;
			break;
		case 3:
			currentOff += frame->getFrameSize() + ID3V2_3_FRAME_HDR_SIZE;
			break;
		case 4:
			currentOff += frame->getFrameSize();
			break;
		}
	}

	tag->setPaddingOffset(currentOff);
	switch (tag->getVersion()[0]) {
	case 3:
		if (!tag->flagIsSet(ID3V2_E_HDR))
			tag->setPaddingSize(tag->getTagSize() - (tag->getPaddingOff() - tag->getFileOff()));
		break;
	}

	return true;
}

void ID3v2_printTag(ID3v2_Tag* tag) {
	printf("ID3v2.%d.%d\n", tag->getVersion()[0], tag->getVersion()[1]);
	printf("Tag size: %d B\n\n", tag->getTagSize());
	
	if (tag->flagIsSet(ID3V2_E_HDR)) {
		printf("Extended header size: %d B\n");
	}

	if (tag->flagIsSet(ID3V2_UNSYNC))
		printf("Unsynchronization\n");
	if (tag->flagIsSet(ID3V2_EXP))
		printf("Experimental Indicator\n");
	if (tag->eFlagIsSet(ID3V2_UPDATE))
		printf("Tag is an update\n");
	if (tag->eFlagIsSet(ID3V2_CRC))
		printf("CRC data present\n");
	if (tag->eFlagIsSet(ID3V2_RESTRICT))	
		printf("Tag restrictions\n");

	printf("Frames found: %d\n", tag->getNumFrames());
	tag->printFrameIDs();

	string s;
	getline(cin, s);
}

// Returns a string equivalent to null-byte-terminated text
// TODO: Add support for Unicode
string ID3v2_textToString(const char* text, unsigned len) {
	char* t = new char[len + 1];
	memcpy(t, text, len);
	t[len] = 0;
	string retValue = t;
	delete t;
	return retValue;
}