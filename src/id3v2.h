#ifndef ID3v2_H
#define ID3v2_H

#include <cstring>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

#define ID3V2_TAG_HDR_SIZE 10
#define ID3V2_2_FRAME_HDR_SIZE 6
#define ID3V2_3_FRAME_HDR_SIZE 10
#define ID3V2_2_FRAME_ID_LEN 3
#define ID3V2_3_FRAME_ID_LEN 4
#define SYNCHSAFE32_MAX 268435455 // 2^28 - 1

enum ID3v2_flag {
	ID3V2_UNSYNC = 0x80,
	ID3V2_E_HDR = 0x40,
	ID3V2_EXP = 0x20,
	ID3V2_FOOTER = 0x10
};

enum ID3v2_eFlag {
	ID3V2_UPDATE = 0x40,
	ID3V2_CRC = 0x20,
	ID3V2_RESTRICT = 0X10
};

enum ID3v2_frameFlag {
	ID3V2_TAG_ALTER = 0x4000,
	ID3V2_FILE_ALTER = 0x2000,
	ID3V2_RD_ONLY = 0x1000,
	ID3V2_GROUP = 0x40,
	ID3V2_COMPRESS = 0x8,
	ID3V2_ENCRYPT = 0x4,
	ID3V2_FRAME_UNSYNC = 0x2,
	ID3V2_DATA_LEN_IND = 0x1
};

typedef unsigned synchsafe32_t;

/****************************************************/
/*                 ID3v2_Frame Base                 */
/****************************************************/

class ID3v2_Frame {

public:

	ID3v2_Frame(unsigned tagVersion, const char* frameID, unsigned frameSize, unsigned flags, streamoff fileOff)
	: m_data(0) {
		m_tagVersion = tagVersion;
		m_flags = flags;
		m_fileOff = fileOff;
		switch(m_tagVersion) {
		case 2:
			memcpy(m_frameID, frameID, ID3V2_2_FRAME_ID_LEN);
			m_frameID[3] = 0;
			m_frameSize = frameSize;
			break;
		case 3:
		case 4:
			memcpy(m_frameID, frameID, ID3V2_3_FRAME_ID_LEN);
			m_frameSize = frameSize;
			break;
		}
	}

	virtual ~ID3v2_Frame() {
		delete[] m_data;
	}

	//////////////////////////////
	// Getters

	const char* getFrameID() const { return m_frameID; }
	unsigned getFrameSize() const { return m_frameSize; }
	bool flagIsSet(ID3v2_frameFlag flag) const { return (m_flags & flag) > 0 ? true : false; }
	const char* getData() const { return m_data; }
	streamoff getFileOff() const { return m_fileOff; }
	unsigned getTagVersion() const { return m_tagVersion; }

	//////////////////////////////
	// Setters

	virtual void setFlag(ID3v2_frameFlag flag) { m_flags |= flag; }
	void setData(const char* data, unsigned len) {
		m_data = new char[len];
		memcpy(m_data, data, len);
	}
	void setFileOff(streamoff off) { m_fileOff = off; }

protected:

	streamoff m_fileOff; // offset of frame in file
	unsigned m_tagVersion;

	//////////////////////////////
	// Frame header data

	char m_frameID[4];
	unsigned m_frameSize;
	unsigned m_flags;

	char* m_data;
};

/****************************************************/
/*                ID3v2_Tag Base                    */
/****************************************************/

class ID3v2_Tag {

public:

	ID3v2_Tag(const char* ver, unsigned flags, unsigned tagSize, streamoff fileOffset) {
		memcpy(m_version, ver, 2);
		m_flags = flags;
		m_tagSize = tagSize;
		m_fileOff = fileOffset;

		// Defaults
		m_paddingOff = fileOffset;
		m_paddingSize = 0;
	}

	virtual ~ID3v2_Tag() {
		for (int i = 0; i < m_frames.size(); i++) {
			delete m_frames[i];
		}
		m_frames.clear();
	}

	//////////////////////////////
	// Getters

	virtual unsigned getEHeaderSize() const =0;
	virtual bool eFlagIsSet(ID3v2_eFlag flag) const =0;

	const char* getVersion() const { return m_version; }
	bool flagIsSet(ID3v2_flag flag) const { return flag & m_flags > 0 ? true : false; }
	unsigned getTagSize() const { return m_tagSize; }
	streamoff getFileOff() const { return m_fileOff; }
	ID3v2_Frame* getFrame(char* frameID) const {
		ID3v2_Frame* retValue = 0;
		char* iFrameID;
		unsigned idLen = m_version[0] > 2 ? ID3V2_3_FRAME_ID_LEN : ID3V2_2_FRAME_ID_LEN;
		iFrameID = new char[idLen + 1];
		for (int i = 0; i < m_frames.size(); i++) {
			memcpy(iFrameID, m_frames[i]->getFrameID(), idLen);
			iFrameID[idLen] = 0; // For C string comparison
			if (strcmp(frameID, iFrameID) == 0)
				retValue = m_frames[i];
		}
		delete iFrameID;
		return retValue;
	}
	size_t getNumFrames() const { return m_frames.size(); }
	unsigned getPaddingSize() const { return m_paddingSize; }
	streamoff getPaddingOff() const { return m_paddingOff; }

	//////////////////////////////
	// Setters

	void setFlag(ID3v2_flag flag) { m_flags |= flag; }
	void setFileOff(streamoff off) { m_fileOff = off; }
	void setPaddingSize(unsigned size) { m_paddingSize = size; }
	void setPaddingOffset(streamoff off) { m_paddingOff = off; }

	//////////////////////////////
	// Utilities

	void addFrame(ID3v2_Frame* frame) {
		m_frames.push_back(frame);
	}

	void printFrameIDs() {

		char* id = 0;
		if (m_version[0] == 3) {
			id = new char[ID3V2_3_FRAME_ID_LEN + 1];
			for (int i = 0; i < m_frames.size(); i++) {
				memcpy(id, m_frames[i]->getFrameID(), ID3V2_3_FRAME_ID_LEN);
				id[4] = '\0';
				printf("%s\n", id);
			}
		} else
			return;

		delete id;
	}

protected:

	streamoff m_fileOff; // offset of tag in file
	streamoff m_paddingOff; // offset of padding in file

	//////////////////////////////
	// Header data

	char m_version[2];
	unsigned m_flags;
	unsigned m_tagSize;

	//////////////////////////////
	// Frames
	vector<ID3v2_Frame*> m_frames;

	//////////////////////////////
	// Padding

	unsigned m_paddingSize;
};










/* ID3v2_3 Tag Structure

+-----------------------------+
|      Header (10 bytes)      |
+-----------------------------+
|       Extended Header       |
| (variable length, OPTIONAL) |
+-----------------------------+
|   Frames (variable length)  |
+-----------------------------+
|           Padding           |
| (variable length, OPTIONAL) |
+-----------------------------+

*/

/************************************************/
/*                ID3v2_3_Tag                   */
/************************************************/

class ID3v2_3_Tag : public ID3v2_Tag {

public:

	ID3v2_3_Tag(const char* ver, unsigned flags, unsigned tagSize, streamoff fileOffset)
	: ID3v2_Tag(ver, flags, tagSize, fileOffset) {}

	//////////////////////////////
	// Getters

	virtual bool eFlagIsSet(ID3v2_eFlag flag) const { return m_eFlags & flag > 0 ? true : false; }
	virtual unsigned getEHeaderSize() const { return m_eHeaderSize; }
	
	//////////////////////////////
	// Setters

	void setEHeaderSize(unsigned eHeaderSize) { m_eHeaderSize = eHeaderSize; }
	void setEFlag(ID3v2_eFlag flag) { m_eFlags |= flag; }
	void setEFlags(unsigned eFlags) { m_eFlags = eFlags; }

protected:

	//////////////////////////////
	// Extended header data
	unsigned m_eHeaderSize;
	unsigned m_eFlags;
};





/****************************************************/
/*                   Utilities                      */
/****************************************************/

unsigned uintToSynchsafe(unsigned i);
unsigned synchsafeToUint(unsigned s);
unsigned synchsafeToUint(const char* s);

bool ID3v2_findTag(streampos& pos, fstream& file);
ID3v2_Tag* ID3v2_parseTag(fstream& file);
bool ID3v2_parseFrames(ID3v2_Tag* tag, char* framePtr);
void ID3v2_printTag(ID3v2_Tag* tag);
string ID3v2_textToString(const char* text, unsigned len);

#endif