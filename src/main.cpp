#include <cstdio>
#include <fstream>
#include <iostream>
#include <string>

#include "id3v2.h"
#include "id3v2_frames.h"

using namespace std;

string getFilePath() {
	string s;
	printf("Please provide the path to an MP3 file\n>> ");
	getline(cin, s);
	return s;
}

int main(int argc, char** argv) {

	string fp;

	if (argc >= 2) {
		fp = argv[1];
	} else {
		fp = getFilePath();
	}

	// --- File extension checking --- //
	bool validFile = false;
	while (!validFile) {
		const char* c = fp.c_str();
		int counter = 0;
		while (*c != '\0') {
			counter++;
			c++;
		}
		if (fp[counter-4] != '.' || fp[counter-3] != 'm' || fp[counter-2] != 'p' || fp[counter-1] != '3') {
			fp = getFilePath();
		} else
			validFile = true;
	}
	// ------------------------------- //

	// Open the file
	fstream file(fp.c_str(), ios::in | ios::out | ios::binary);
	if (!file.is_open()) {
		printf("Error opening the file.\nPRESS ENTER TO EXIT");
		getline(cin, fp);
		return 1;
	}

	// Test the parsing
	ID3v2_Tag* tag = ID3v2_parseTag(file);
	//ID3v2_printTag(tag);

	ID3v2_Frame_Text* title = dynamic_cast<ID3v2_Frame_Text*>(tag->getFrame("TIT2"));
	ID3v2_Frame_Text* album = dynamic_cast<ID3v2_Frame_Text*>(tag->getFrame("TALB"));
	ID3v2_Frame_Text* artist = dynamic_cast<ID3v2_Frame_Text*>(tag->getFrame("TPE1"));

	printf("%s\n%s - %s\n", ID3v2_textToString(title->getData(), title->getFrameSize() - 1).c_str(), ID3v2_textToString(artist->getData(), artist->getFrameSize() - 1).c_str(), ID3v2_textToString(album->getData(), album->getFrameSize() - 1).c_str());
	delete tag;

	if (file.is_open())
		file.close();
	return 0;
}